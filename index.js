import domtoimage from "dom-to-image";

const list = new Array;
for (const i = 0; i< 100 ; i++) {
    list.push("공급자 " + i);
}

const dataUrltoFile = (dataUrl, fileName) => {
    const arr = String(dataUrl).split(",");
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], fileName, { type: mime });
  };

const downloadImg = async function(node, fileName) {
    const dataUrl = await domtoimage.toJpeg(node)
    return dataUrltoFile(dataUrl, fileName);
};
